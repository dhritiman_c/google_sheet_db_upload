import mysql.connector
import db_config as dc

def GetSpreadsheetData(sheetName, worksheetIndex,client):
    sheet = client.open(sheetName).get_worksheet(worksheetIndex)
    return sheet.get_all_values()[1:]


def PreserveNULLValues(listName):
    print('Preserving NULL values…')
    for x in range(len(listName)):
        for y in range(len(listName[x])):
            if listName[x][y] == '':
                listName[x][y] = None
    print('NULL values preserved.')


def WriteToMySQLTable(sql_data,tableName):
    try:
        connection = mysql.connector.connect(
            user=dc.user,
            password=dc.password,
            host=dc.host,
            database=dc.database
        )

        sql_drop = "DROP TABLE IF EXISTS {}".format(tableName)

        sql_create_table = """
        CREATE TABLE {}(
        Date VARCHAR(15),
	Retailer_Smart_ID INTEGER(11),
        Retailer_Name VARCHAR(100),
        Retailer_Mobile VARCHAR(14),
	LAPU_Number VARCHAR(50),
	City VARCHAR(20),
	SO_Name VARCHAR(80),
        Non_Conversion_Reason VARCHAR(80)
        )""".format(tableName)

        # sql_create_test_table="""CREATE TABLE {}(
        # ID int(10),
        # Field VARCHAR(30),
        # abc VARCHAR(10),
        # col VARCHAR(10)
        # )""".format(tableName)

        sql_insert_statement = """INSERT INTO {}(
        Date,
	Retailer_Smart_ID,
        Retailer_Name,
        Retailer_Mobile,
	LAPU_Number,
	City,
	SO_Name,
        Non_Conversion_Reason)
        VALUES(%s,%s,%s,%s,%s,%s,%s,%s)""".format(tableName)

        # sql_insert_test_statement ="""INSERT INTO {} (
        # ID int(10),
        # Field VARCHAR(30),
        # abc VARCHAR(10),
        # col VARCHAR(10)
        # ) VALUES( % s, % s, % s, % s )""".format(tableName)

        cursor = connection.cursor()
        cursor.execute(sql_drop)
        cursor.execute(sql_create_table)
        print('Table {} has been created'.format(tableName))

        for i in sql_data:
            cursor.execute(sql_insert_statement, i)

        connection.commit()
        print("Table {} successfully updated".format(tableName))

    except mysql.connector.Error as error:
        connection.rollback()

        print("Error: {}.Table {} not updated!".format(error, tableName))

    finally:
        cursor.execute('SELECT COUNT(*) FROM {}'.format(tableName))

        rowCount = cursor.fetchone()[0]
        print(tableName, 'row count:', rowCount)
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed.")
